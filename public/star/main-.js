var __extends = this && this.__extends || function() {
        var e = Object.setPrototypeOf || {
            __proto__: []
        }
        instanceof Array && function(e, t) {
            e.__proto__ = t
        } || function(e, t) {
            for (var a in t) t.hasOwnProperty(a) && (e[a] = t[a])
        };
        return function(t, a) {
            function r() {
                this.constructor = t
            }
            e(t, a), t.prototype = null === a ? Object.create(a) : (r.prototype = a.prototype, new r)
        }
    }(),
    CelestialSphere = function(e) {
        function t(t, a, r, n, o, i, s, d, l) {
            var h = e.call(this, t, a) || this;
            h._twinkleStars = !0, h._starData = r, h._radius = n, h._starLimit = o, h._starScale = i, h._showAsterisms = s, h._asterismColor = d, h._twinkleStars = l;
            var p = new BABYLON.Mesh("starMesh", a);
            p.parent = h, p.alphaIndex = 20;
            for (var c = [], B = [], x = [], u = [], g = [], L = 0, w = Math.min(r.rightAscension.length, h._starLimit), O = 0; O < w; O++) {
                var A = h._starData.rightAscension[O],
                    N = h._starData.declination[O],
                    v = h._starScaleFactor(O),
                    _ = new BABYLON.Vector3(0 * v, .7 * v, h._radius),
                    C = new BABYLON.Vector3(-.5 * v, -.3 * v, h._radius),
                    f = new BABYLON.Vector3(.5 * v, -.3 * v, h._radius),
                    m = BABYLON.Matrix.RotationYawPitchRoll(-A, -N, 0);
                _ = BABYLON.Vector3.TransformCoordinates(_, m), C = BABYLON.Vector3.TransformCoordinates(C, m), f = BABYLON.Vector3.TransformCoordinates(f, m), c.push(_.x, _.y, _.z, C.x, C.y, C.z, f.x, f.y, f.z);
                var Y = h._starColor(O);
                x.push(Y.r, Y.g, Y.b, Y.a, Y.r, Y.g, Y.b, Y.a, Y.r, Y.g, Y.b, Y.a), u.push(.5, 1, 0, 0, 1, 0);
                var b = Math.random(),
                    S = Math.random();
                g.push(b, S, b, S, b, S), B.push(L, L + 1, L + 2), L += 3
            }
            var T = new BABYLON.VertexData;
            T.positions = c, T.indices = B, T.colors = x, T.uvs = u, T.uvs2 = g, T.applyToMesh(p);
            var I = new BABYLON.StandardMaterial("starMaterial", a),
                k = new BABYLON.Texture("star.png", a);
            if (I.opacityTexture = k, I.disableLighting = !0, p.material = I, h._twinkleStars) {
                var M = new BABYLON.Texture("noise.png", a);
                I.emissiveTexture = M, M.coordinatesIndex = 1, a.registerBeforeRender(function() {
                    M.uOffset += .008
                })
            } else I.emissiveColor = new BABYLON.Color3(1, 1, 1);
            //if (h._showAsterisms)
            if (false)
                for (var G = [], U = 0; U < h._starData.asterismIndices.length; U++) {
                    if (-1 != (O = h._starData.asterismIndices[U])) {
                        A = h._starData.rightAscension[O], N = h._starData.declination[O];
                        var y = h._radius * Math.cos(N) * Math.sin(A),
                            V = h._radius * Math.sin(N),
                            D = h._radius * Math.cos(N) * Math.cos(A);
                        G.push(new BABYLON.Vector3(-y, V, D))
                    } else {
                        var z = BABYLON.Mesh.CreateLines("asterismLines", G, a);
                        z.color = h._asterismColor, z.parent = h, z.alphaIndex = 10, G = []
                    }
                }
            new BABYLON.Color3(1, 0, 0);
            var R = [];
            for (U = 0; U < 101; U++) {
                var F = 2 * Math.PI * U / 100;
                y = Math.cos(F) * h._radius, V = 0, D = Math.sin(F) * h._radius;
                R.push(new BABYLON.Vector3(y, V, D))
            }
            n += 20;
            var P = 2 * Math.PI * n / 4 / 2,
                E = [new BABYLON.Vector3(0, 0, P), new BABYLON.Vector3(0, 0, -P)],
                H = new BABYLON.TransformNode("tubeParentXform", a),
                W = new BABYLON.TransformNode("tubeChildXform", a),
                j = BABYLON.MeshBuilder.CreateTube("tube", {
                    path: E,
                    radius: n,
                    sideOrientation: BABYLON.Mesh.BACKSIDE,
                    updatable: !1
                }, a);
            j.alphaIndex = 0;
            var X = new BABYLON.Texture("eso0932a.png", a, !0, !1);
            X.vScale = -1, j.parent = W, W.parent = H, j.rotate(new BABYLON.Vector3(0, 0, -1), .57), W.rotate(new BABYLON.Vector3(1, 0, 0), .48), H.rotate(new BABYLON.Vector3(0, -1, 0), .22);
            var J = new BABYLON.StandardMaterial("skyBox", a);
            return J.backFaceCulling = !0, J.disableLighting = !0, J.emissiveTexture = X, j.material = J, j.material.alpha = .5, H.parent = h, h
        }
        return __extends(t, e), t.prototype._starColor = function(e) {
            var t = BABYLON.Scalar.Normalize(this._starData.colorIndexBV[e], -.4, 2),
                a = this._starData.color.length - 1,
                r = Math.round(a * t);
            r = BABYLON.Scalar.Clamp(r, 0, a);
            var n = this._starData.color[r];
            return new BABYLON.Color4(n[0], n[1], n[2], 0)
        }, t.prototype._starScaleFactor = function(e) {
            return (8 - this._starData.apparentMagnitude[e]) * this._starScale
        }, t
    }(BABYLON.TransformNode),
    WebGL = function() {
        function e(e) {
            this._canvas = document.getElementById(e), this._engine = new BABYLON.Engine(this._canvas, !0)
        }
        return e.prototype.createScene = function() {
            var e = this;
            this._scene = new BABYLON.Scene(this._engine), this._scene.clearColor = new BABYLON.Color4(0, 0, 0, 1);
            var t, a, r = new BABYLON.AssetsManager(this._scene),
                n = r.addTextFileTask("star-data", "star-data.json"),
                o = 5e3,
                i = 1,
                s = 300,
                d = !0,
                l = new BABYLON.Color3(0, 0, .7),
                h = !0;
            n.onSuccess = function(r) {
                t = JSON.parse(r.text), a = new CelestialSphere("celestialSphere", e._scene, t, s, o, i, d, l, h)
            }, r.load();
			
            // var p = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI"),
                // c = new BABYLON.GUI.StackPanel;
            // c.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT, c.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP, c.paddingTop = "20px", c.paddingRight = "20px", c.width = "220px", c.height = "700px", c.background = "#3338", p.addControl(c);
            // var B = new BABYLON.GUI.TextBlock;
            // B.text = "Celestial Sphere", B.fontSize = "22px", B.height = "40px", B.color = "white", B.paddingTop = "20px", c.addControl(B);
            // var x = new BABYLON.GUI.TextBlock;
            // x.text = "Number of stars: " + o.toFixed(0), x.fontSize = "15px", x.height = "40px", x.color = "white", x.paddingTop = "20px", c.addControl(x);
            // var u = new BABYLON.GUI.Slider;
            // u.minimum = 100, u.maximum = 5e3, u.value = o, u.width = "200px", u.height = "30px", u.thumbWidth = "15px", u.color = "green", u.borderColor = "white", u.background = "black", u.paddingTop = "7px", u.paddingLeft = "10px", u.paddingRight = "10px", u.onValueChangedObservable.add(function(e) {
                // x.text = "Number of stars: " + e.toFixed(0), o = e
            // }), c.addControl(u);
            // var g = new BABYLON.GUI.TextBlock;
            // g.text = "Star scale: " + i.toFixed(1), g.fontSize = "15px", g.height = "40px", g.color = "white", g.paddingTop = "20px", c.addControl(g);
            // var L = new BABYLON.GUI.Slider;
            // L.minimum = .1, L.maximum = 2, L.value = i, L.width = "200px", L.height = "30px", L.thumbWidth = "15px", L.color = "green", L.borderColor = "white", L.background = "black", L.paddingTop = "7px", L.paddingLeft = "10px", L.paddingRight = "10px", L.onValueChangedObservable.add(function(e) {
                // g.text = "Star scale: " + e.toFixed(1), i = e
            // }), c.addControl(L);
            // var w = new BABYLON.GUI.TextBlock;
            // w.text = "Sphere radius: " + s.toFixed(0), w.fontSize = "15px", w.height = "40px", w.color = "white", w.paddingTop = "20px", c.addControl(w);
            // var O = new BABYLON.GUI.Slider;
            // O.minimum = 100, O.maximum = 900, O.value = s, O.width = "200px", O.height = "30px", O.thumbWidth = "15px", O.color = "green", O.borderColor = "white", O.background = "black", O.paddingTop = "7px", O.paddingLeft = "10px", O.paddingRight = "10px", O.onValueChangedObservable.add(function(e) {
                // w.text = "Sphere radius: " + e.toFixed(0), s = e
            // }), c.addControl(O);
            // var A = new BABYLON.GUI.Checkbox;
            // A.width = "20px", A.height = "20px", A.isChecked = h, A.color = "green", A.onIsCheckedChangedObservable.add(function(e) {
                // h = e
            // });
            // var N = BABYLON.GUI.Control.AddHeader(A, " Twinkle stars", "180px", {
                // isHorizontal: !0,
                // controlFirst: !0
            // });
            // N.height = "60px", N.paddingLeft = "20px", N.fontSize = "15px", N.paddingTop = "20px", N.color = "white", N.children[1].onPointerDownObservable.add(function() {
                // A.isChecked = !A.isChecked
            // }), c.addControl(N);
            // var v = new BABYLON.GUI.Checkbox;
            // v.width = "20px", v.height = "20px", v.isChecked = d, v.color = "green", v.onIsCheckedChangedObservable.add(function(e) {
                // d = e
            // });
            // var _ = BABYLON.GUI.Control.AddHeader(v, " Show asterisms", "180px", {
                // isHorizontal: !0,
                // controlFirst: !0
            // });
            // _.height = "60px", _.paddingLeft = "20px", _.fontSize = "15px", _.paddingTop = "10px", _.color = "white", _.children[1].onPointerDownObservable.add(function() {
                // v.isChecked = !v.isChecked
            // }), c.addControl(_);
            // var C = new BABYLON.GUI.TextBlock;
            // C.text = "Asterism color", C.fontSize = "15px", C.height = "40px", C.color = "white", c.addControl(C);
            // var f = new BABYLON.GUI.ColorPicker;
            // f.value = l, f.height = "200px", f.width = "200px", f.paddingLeft = "10px", f.paddingRight = "10px", f.onValueChangedObservable.add(function(e) {
                // l = e
            // }), c.addControl(f);
            // var m = BABYLON.GUI.Button.CreateSimpleButton("regenButton", "Regenerate!");
            // m.width = "200px", m.height = "70px", m.color = "white", m.fontSize = "18px", m.background = "green", m.paddingLeft = "20px", m.paddingRight = "20px", m.paddingTop = "10px", m.paddingBottom = "20px", m.onPointerUpObservable.add(function() {
                // a.dispose(), a = new CelestialSphere("celestialSphere", e._scene, t, s, o, i, d, l.clone(), h)
            // }), c.addControl(m), 
			this._scene.createDefaultCameraOrLight(!0, !0, !0), new BABYLON.GlowLayer("glow", this._scene).intensity = 1
        }, e.prototype.renderScene = function() {
            var e = this;
            this._engine.runRenderLoop(function() {
                e._scene.render()
            }), window.addEventListener("resize", function() {
                e._engine.resize()
            })
        }, e
    }();
window.addEventListener("DOMContentLoaded", function() {
    var e = new WebGL("render-canvas");
    e.createScene(), e.renderScene()
});