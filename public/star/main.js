var __extends = this && this.__extends || function() {
    var e = Object.setPrototypeOf || {
        __proto__: []
    }
    instanceof Array && function(e, t) {
        e.__proto__ = t
    } || function(e, t) {
        for (var a in t) t.hasOwnProperty(a) && (e[a] = t[a])
    };
    return function(t, a) {
        function r() {
            this.constructor = t
        }
        e(t, a), t.prototype = null === a ? Object.create(a) : (r.prototype = a.prototype, new r)
    }
}();
CelestialSphere = function(e) {
    function t(t, a, r, n, o, i, s, d, l) {
        var h = e.call(this, t, a) || this;
        h._twinkleStars = !0, h._starData = r, h._radius = n, h._starLimit = o, h._starScale = i, h._showAsterisms = s, h._asterismColor = d, h._twinkleStars = l;
        var p = new BABYLON.Mesh("starMesh", a);
        p.parent = h, p.alphaIndex = 20;
        for (var c = [], B = [], x = [], u = [], g = [], L = 0, w = Math.min(r.rightAscension.length, h._starLimit), O = 0; O < w; O++) {
            var A = h._starData.rightAscension[O],
                N = h._starData.declination[O],
                v = h._starScaleFactor(O),
                _ = new BABYLON.Vector3(0 * v, .7 * v, h._radius),
                C = new BABYLON.Vector3(-.5 * v, -.3 * v, h._radius),
                f = new BABYLON.Vector3(.5 * v, -.3 * v, h._radius),
                m = BABYLON.Matrix.RotationYawPitchRoll(-A, -N, 0);
            _ = BABYLON.Vector3.TransformCoordinates(_, m), C = BABYLON.Vector3.TransformCoordinates(C, m), f = BABYLON.Vector3.TransformCoordinates(f, m), c.push(_.x, _.y, _.z, C.x, C.y, C.z, f.x, f.y, f.z);
            var Y = h._starColor(O);
            x.push(Y.r, Y.g, Y.b, Y.a, Y.r, Y.g, Y.b, Y.a, Y.r, Y.g, Y.b, Y.a), u.push(.5, 1, 0, 0, 1, 0);
            var b = Math.random(),
                S = Math.random();
            g.push(b, S, b, S, b, S), B.push(L, L + 1, L + 2), L += 3
        }
        var T = new BABYLON.VertexData;
        T.positions = c, T.indices = B, T.colors = x, T.uvs = u, T.uvs2 = g, T.applyToMesh(p);
        var I = new BABYLON.StandardMaterial("starMaterial", a),
            k = new BABYLON.Texture("star.png", a);
        if (I.opacityTexture = k, I.disableLighting = !0, p.material = I, h._twinkleStars) {
            var M = new BABYLON.Texture("noise.png", a);
            I.emissiveTexture = M,
                M.coordinatesIndex = 1, a.registerBeforeRender(function() {
                    M.uOffset += .008
                })
        } else I.emissiveColor = new BABYLON.Color3(1, 1, 1);

        new BABYLON.Color3(1, 0, 0);
        var R = [];
        for (U = 0; U < 101; U++) {
            var F = 2 * Math.PI * U / 100;
            y = Math.cos(F) * h._radius, V = 0, D = Math.sin(F) * h._radius;
            R.push(new BABYLON.Vector3(y, V, D))
        }
        n += 20;
        var P = 2 * Math.PI * n / 4 / 2,
            E = [new BABYLON.Vector3(0, 0, P), new BABYLON.Vector3(0, 0, -P)],
            H = new BABYLON.TransformNode("tubeParentXform", a),
            W = new BABYLON.TransformNode("tubeChildXform", a),
            j = BABYLON.MeshBuilder.CreateTube("tube", {
                path: E,
                radius: n,
                sideOrientation: BABYLON.Mesh.BACKSIDE,
                updatable: !1
            }, a);
        j.alphaIndex = 0;
        var X = new BABYLON.Texture("eso0932a.png", a, !0, !1);
        X.vScale = -1, j.parent = W, W.parent = H, j.rotate(new BABYLON.Vector3(0, 0, -1), .57), W.rotate(new BABYLON.Vector3(1, 0, 0), .48), H.rotate(new BABYLON.Vector3(0, -1, 0), .22);
        var J = new BABYLON.StandardMaterial("skyBox", a);
        return J.backFaceCulling = !0, J.disableLighting = !0, J.emissiveTexture = X, j.material = J, j.material.alpha = .5, H.parent = h, h
    }
    return __extends(t, e), t.prototype._starColor = function(e) {
        var t = BABYLON.Scalar.Normalize(this._starData.colorIndexBV[e], -.4, 2),
            a = this._starData.color.length - 1,
            r = Math.round(a * t);
        r = BABYLON.Scalar.Clamp(r, 0, a);
        var n = this._starData.color[r];
        return new BABYLON.Color4(n[0], n[1], n[2], 0)
    }, t.prototype._starScaleFactor = function(e) {
        return (8 - this._starData.apparentMagnitude[e]) * this._starScale
    }, t
}(BABYLON.TransformNode);
WebGL = function() {
    function e(e) {
        this._canvas = document.getElementById(e), this._engine = new BABYLON.Engine(this._canvas, !0)
    }
    return e.prototype.createScene = function() {
        var e = this;
        this._scene = new BABYLON.Scene(this._engine), this._scene.clearColor = new BABYLON.Color4(0, 0, 0, 1);
        var t, a, r = new BABYLON.AssetsManager(this._scene),
            n = r.addTextFileTask("star-data", "star-data.json"),
            o = 5e3,
            i = 1,
            s = 300,
            d = !0,
            l = new BABYLON.Color3(0, 0, .7),
            h = !0;
        n.onSuccess = function(r) {
            t = JSON.parse(r.text), a = new CelestialSphere("celestialSphere", e._scene, t, s, o, i, d, l, h)
        }, r.load();
        this._scene.createDefaultCameraOrLight(!0, !0, !0), new BABYLON.GlowLayer("glow", this._scene).intensity = 1
    }, e.prototype.renderScene = function() {
        var e = this;
        this._engine.runRenderLoop(function() {
            e._scene.render()
        }), window.addEventListener("resize", function() {
            e._engine.resize()
        })
    }, e
}();
window.addEventListener("DOMContentLoaded", function() {
    var e = new WebGL("render-canvas");
    e.createScene(), e.renderScene()
});