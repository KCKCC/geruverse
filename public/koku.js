buildKo = (scene, camera, BABYLON) => {
    var texture = new BABYLON.Texture("textures/ko.png", scene);
    var mat = new BABYLON.StandardMaterial("komimat", scene);
    mat.emissiveTexture = mat.opacityTexture = texture;
    mat.emissiveTexture.hasAlpha = true;

    mat.disableLighting = true;
    mat.alpha = 1;
    mat.backFaceCulling = false;

    var komi = BABYLON.MeshBuilder.CreatePlane("komi", { width: 8.79 / 2, height: 20.22 / 2 }, scene);
    // komi.billboardMode = BABYLON.Mesh.BILLBOARDMODE_Y;
    komi.setPivotMatrix(BABYLON.Matrix.Translation(0, (10.22 / 2), 0), false);
    // komi.position=new BABYLON.Vector3(0, 10.11, 0);
    komi.material = mat;
    komi.position.x = 3;
    komi.lookAt(camera.position);
    var aniKoR = new BABYLON.Animation("myAnimation", "rotation.z", 30, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var keys = [];

    keys.push({
        frame: 0,
        value: -0.1
    });

    keys.push({
        frame: 20,
        value: 0.1
    });

    keys.push({
        frame: 40,
        value: -0.1
    });
    aniKoR.setKeys(keys);

    var aniKoP = new BABYLON.Animation("myAnimation", "position", 30, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var keys = [];

    keys.push({
        frame: 0,
        value: new BABYLON.Vector3(3, 0, 0),
    });

    keys.push({
        frame: 8,
        value: new BABYLON.Vector3(3, 1, 0),
    });

    keys.push({
        frame: 17,
        value: new BABYLON.Vector3(3, 0, 0),
    });
    aniKoP.setKeys(keys);
    // komi.rotation.z=-0.1;
    komi.animations = [];
    komi.animations.push(aniKoR);
    komi.animations.push(aniKoP);
    scene.beginAnimation(komi, 0, 100, true);
    komi.setEnabled (false);
    komi.isPickable = false;
    return komi;
}

buildKu = (scene, camera, BABYLON) => {
    var texture = new BABYLON.Texture("textures/ku.png", scene);
    var mat = new BABYLON.StandardMaterial("kumimat", scene);
    mat.emissiveTexture = mat.opacityTexture = texture;
    mat.emissiveTexture.hasAlpha = true;

    mat.disableLighting = true;
    mat.alpha = 1;
    mat.backFaceCulling = false;

    var kumi = BABYLON.MeshBuilder.CreatePlane("kumi", { width: 8.79 / 2, height: 20.22 / 2 }, scene);
    // kumi.billboardMode = BABYLON.Mesh.BILLBOARDMODE_Y;
    kumi.setPivotMatrix(BABYLON.Matrix.Translation(0, (10.22 / 2), 0), false);
    // kumi.position=new BABYLON.Vector3(0, 10.11, 0);
    kumi.material = mat;
    kumi.position.x = 3;
    kumi.lookAt(camera.position);
    var aniKoR = new BABYLON.Animation("myAnimation", "rotation.z", 30, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var keys = [];

    keys.push({
        frame: 0,
        value: -0.1
    });

    keys.push({
        frame: 30,
        value: 0.1
    });

    keys.push({
        frame: 60,
        value: -0.1
    });
    aniKoR.setKeys(keys);

    var aniKoP = new BABYLON.Animation("myAnimation", "position", 30, BABYLON.Animation.ANIMATIONTYPE_VECTOR3, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
    var keys = [];

    keys.push({
        frame: 0,
        value: new BABYLON.Vector3(-3, 0, 0),
    });

    keys.push({
        frame: 8,
        value: new BABYLON.Vector3(-3, 1, 0),
    });

    keys.push({
        frame: 17,
        value: new BABYLON.Vector3(-3, 0, 0),
    });
    aniKoP.setKeys(keys);
    // kumi.rotation.z=-0.1;
    kumi.animations = [];
    kumi.animations.push(aniKoR);
    kumi.animations.push(aniKoP);
    scene.beginAnimation(kumi, 0, 100, true);
    kumi.isPickable = false;
    kumi.setEnabled (false);
    return kumi;
}

lookAt = (tM, lAt) => {
	/*
	 * tM = mesh to rotate.
	 * lAt = vector3(xyz) of target position to look at
	 */

    lAt = lAt.subtract(tM.position);
    tM.rotation.y = -Math.atan2(lAt.z, lAt.x) - Math.PI / 2;
}